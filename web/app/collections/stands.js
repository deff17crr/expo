App.Collections.Stands = Backbone.Collection.extend({
    model : App.Models.Stand,
    initialize: function (options) {
        this.url = options.url;
    }
});