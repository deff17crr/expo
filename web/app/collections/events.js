App.Collections.Events = Backbone.Collection.extend({
    model : App.Models.Event,
    url : Routing.generate('get_events')
});