(function ($) {
    App.Router = Backbone.Router.extend({
        'routes' : {
            '' : 'index',
            'event/:event_id/stands' : 'stands',
            'reservation-stand/:stand_id' : 'reservationStand'
        },
        index : function () {
            new App.Views.Events({
                collection: new App.Collections.Events()
            });
        },
        stands: function (event_id) {
            var collection = new App.Collections.Stands({url: Routing.generate('get_stands_event', {event: event_id})});
            new App.Views.Stands({
                collection : collection
            });
        },
        reservationStand: function (stand_id) {
            new App.Views.ReservationStand({
                model: new App.Models.Stand({url: Routing.generate('get_stand', {stand : stand_id})})
            });
        }
    });

    App.InitializedRouter = new App.Router;
    Backbone.history.start();
})(jQuery);