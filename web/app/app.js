window.App = {
    Models: {},
    Collections: {},
    Views: {},
    Router : null,
    InitializedRouter : null,
    template: function(id) {
        return _.template( $(id).html());
    }
};