App.Models.Stand = Backbone.Model.extend({
    initialize : function (options) {
        if ('url' in options) {
            this.url = options.url;
        }
    }
});