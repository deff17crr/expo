App.Views.Event = Backbone.View.extend({
    initialize: function (options) {
        this.svg = options.svg;
        this.render();
    },
    events: {
        'mouseover' : 'hover',
        'mouseout' : 'mouseout',
        'click' : 'click'
    },
    hover : function () {
        this.getCurrentDistrict().addClass('hovered');

        /* render gray info bar above event icon on map on mouseover */
        this.renderInfoBar()
    },
    mouseout : function () {
        this.getCurrentDistrict().removeClass('hovered');
        this.svg.find('.event-info').remove();
    },
    click : function () {
        this.svg.find('.selected').removeClass('selected');
        this.getCurrentDistrict().addClass('selected');
        this.$el.addClass('selected');

        /* re render sidebar with data from selected event  */
        new App.Views.EventsSidebar({model: this.model});
    },
    render: function () {
        /* find district on svg map */
        var districtElement = this.getCurrentDistrict();

        /* calculate position for event icon */
        var bBox = districtElement[0].getBBox();
        var x = (bBox.x + bBox.width / 2) - 30;
        var y = (bBox.y + bBox.height / 2) - 30;

        var numb_events = !districtElement.attr('numb-events') ? 0 : parseInt(districtElement.attr('numb-events'));

        /* render event icon */
        var svgEvent = document.createElementNS('http://www.w3.org/2000/svg', 'image');
        svgEvent.setAttributeNS('', 'height', '50');
        svgEvent.setAttributeNS('', 'class', 'event');
        svgEvent.setAttributeNS('', 'width', '50');
        svgEvent.setAttributeNS('http://www.w3.org/1999/xlink', 'href', '/assets/images/event.png');
        svgEvent.setAttributeNS('', 'x', x + (numb_events * 15) + '');
        svgEvent.setAttributeNS('', 'y', y + (numb_events * 2) + '');
        svgEvent.setAttributeNS('', 'visibility', 'visible');
        svgEvent.setAttributeNS('', 'district-short_code', this.model.get('district_short_code'));
        this.setElement(svgEvent);

        districtElement.attr('numb-events', numb_events + 1)
    },
    renderInfoBar : function () {
        // use the native SVG interface to get the bounding box
        var bBox = this.$el[0].getBBox();
        // return the center of the bounding box
        var x = (bBox.x + bBox.width / 2) - 18;
        var y = (bBox.y + bBox.height / 2) - 115;

        /* change info bar position id it doesn't fit*/
        if (y < 0) {
            y += 125;
        }
        if (x > 440) {
            x -= 163
        }

        /* render rectangle bar for event info */
        var bar = $(document.createElementNS("http://www.w3.org/2000/svg", "rect")).attr({
            x: x,
            y: y,
            width: 200,
            height: 75,
            fill: "#eee",
            class : 'event-info'
        });
        this.svg.append(bar);

        /* render event name */
        var EventTitle = document.createElementNS("http://www.w3.org/2000/svg", "text");
        EventTitle.setAttributeNS('', "x", x + 10);
        EventTitle.setAttributeNS('', "y", y + 25);
        EventTitle.setAttributeNS('', "font-size", "14");
        EventTitle.setAttributeNS('', "font-family", "Arial");
        EventTitle.setAttributeNS('', "class", "event-info");
        EventTitle.setAttributeNS('', "fill", "#000");
        EventTitle.appendChild(document.createTextNode("Event: " + this.model.get('title')));
        this.svg.append(EventTitle);

        /* render district name */
        var DistrictText = document.createElementNS("http://www.w3.org/2000/svg", "text");
        DistrictText.setAttributeNS('', "x", x + 10);
        DistrictText.setAttributeNS('', "y", y + 43);
        DistrictText.setAttributeNS('', "font-size", "12");
        DistrictText.setAttributeNS('', "class", "event-info");
        DistrictText.setAttributeNS('', "fill", "#333");
        DistrictText.appendChild(document.createTextNode("District: " + this.model.get('district_name')));
        this.svg.append(DistrictText);

        /* render event dates */
        var EventDates = document.createElementNS("http://www.w3.org/2000/svg", "text");
        EventDates.setAttributeNS('', "x", x + 10);
        EventDates.setAttributeNS('', "y", y + 62);
        EventDates.setAttributeNS('', "font-size", "10");
        EventDates.setAttributeNS('', "class", "event-info");
        EventDates.setAttributeNS('', "fill", "#333");
        EventDates.appendChild(document.createTextNode("Date: " + this.model.get('start_date') + ' - ' + this.model.get('end_date')));
        this.svg.append(EventDates);
    },
    getCurrentDistrict: function () {
        return this.svg.find('#' + this.model.get('district_short_code'));
    }
});