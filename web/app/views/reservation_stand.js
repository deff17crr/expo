App.Views.ReservationStand = Backbone.View.extend({
    el: '#container',
    template: App.template('#template-reservation-stand'),
    initialize: function () {
        this.render();
    },
    events : {
        'click .submit' : 'submit'
    },
    submit: function (e) {
        var self = this;
        self.getReservationForm().ajaxForm({
            forceSync: true,
            error: function (response) {
                if (response.status == '400' && typeof response.responseJSON.formErrors !== 'undefined') {
                    self.renderErrors(response.responseJSON.formErrors);
                }
            },
            success: function () {
                App.InitializedRouter.navigate('event/'+ self.model.get('event').id +'/stands', true);
            }
        });
        e.preventDefault();
        self.getReservationForm().trigger('submit');
    },
    render: function () {
        /* render stand */
        var self = this;

        this.model.fetch({
            success: function (data) {
                self.$el.html(self.template(data.toJSON()));
                self.$el.find('.fancy').fancybox();
                self.getReservationForm()
                    .attr('action', Routing.generate('patch_stand_reservation', {stand: data.id}))
                ;
            }
        });
    },
    renderErrors: function (formErrors) {
        var reservationForm = this.getReservationForm();
        reservationForm.find('.has-error').removeClass('has-error');
        reservationForm.find('.text-danger').remove();

        for (var fieldName in formErrors) {
            var field = reservationForm.find('[name="company[' + fieldName + ']"]');

            field.closest('.form-group').addClass('has-error');
            field.before('<p class="text-danger">' + formErrors[fieldName].join(', ') +'</p>')
        }
    },
    getReservationForm: function () {
        return this.$el.find('.reservation-stand-form');
    }
});