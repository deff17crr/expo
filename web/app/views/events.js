App.Views.Events = Backbone.View.extend({
    el: '#container',
    template: App.template('#template-event-map'),
    initialize: function () {
        this.render();
    },
    render: function () {
        /* render map */
        this.$el.html(this.template());
        var collection = this.collection;
        
        /* render sidebar */
        new App.Views.EventsSidebar({});
        

        var svg = this.$el.find('#event-map').find('svg');
        
        /* render events on loaded svg map */
        collection.fetch({success: function (collection) {
            collection.each(function (event) {
                var EventView = new App.Views.Event({
                    model: event,
                    svg: svg
                });

                svg.append(EventView.$el);
            });
        }});
    }
});