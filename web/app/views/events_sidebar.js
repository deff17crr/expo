App.Views.EventsSidebar = Backbone.View.extend({
    el: '#event-map-info',
    template: App.template('#template-event-map-info'),
    initialize: function () {
        this.render();
    },
    render: function () {
        /* render map */
        this.$el.html(this.template(this.model == null ? {} : this.model.toJSON()));
    }
});