App.Views.Stands = Backbone.View.extend({
    el: '#container',
    template: App.template('#template-stands-page'),
    initialize: function () {
        this.render();
    },
    render: function () {
        /* render map */
        this.$el.html(this.template());

        /* render list of stands */
        this.collection.fetch({
            success: function (collection) {
                collection.each(function (stand) {
                    new App.Views.Stand({model: stand});
                });
            },
            error: function (data, response, b) {
                if (response.status == 404) {
                    App.InitializedRouter.navigate('#', true);
                }
            }
        });
    }

});