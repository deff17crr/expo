App.Views.Stand = Backbone.View.extend({
    el: '#stands',
    template: App.template('#template-stand-item'),
    initialize: function () {
        this.render();
    },
    render: function () {
        /* render stand */
        this.$el.append(this.template(this.model.toJSON()));
        this.$el.find('.fancy').fancybox();
    }
});