<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\District;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadDistrictData
 */
class LoadDistrictData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $district = new District();
        $district->setName('Anenii Noi');
        $district->setShortCode('MD-AN');
        $manager->persist($district);

        $district = new District();
        $district->setName('Balti');
        $district->setShortCode('MD-BA');
        $manager->persist($district);

        $district = new District();
        $district->setName('Tighina');
        $district->setShortCode('MD-BD');
        $manager->persist($district);

        $district = new District();
        $district->setName('Briceni');
        $district->setShortCode('MD-BR');
        $manager->persist($district);

        $district = new District();
        $district->setName('Basarabeasca');
        $district->setShortCode('MD-BS');
        $manager->persist($district);

        $district = new District();
        $district->setName('Cahul');
        $district->setShortCode('MD-CA');
        $manager->persist($district);
        $this->addReference(1, $district);

        $district = new District();
        $district->setName('Calarasi');
        $district->setShortCode('MD-CL');
        $manager->persist($district);

        $district = new District();
        $district->setName('Cimislia');
        $district->setShortCode('MD-CM');
        $manager->persist($district);

        $district = new District();
        $district->setName('Criuleni');
        $district->setShortCode('MD-CR');
        $manager->persist($district);

        $district = new District();
        $district->setName('Causeni');
        $district->setShortCode('MD-CS');
        $manager->persist($district);

        $district = new District();
        $district->setName('Cantemir');
        $district->setShortCode('MD-CT');
        $manager->persist($district);

        $district = new District();
        $district->setName('Chisinau');
        $district->setShortCode('MD-CU');
        $manager->persist($district);

        $this->addReference(2, $district);

        $district = new District();
        $district->setName('Donduseni');
        $district->setShortCode('MD-DO');
        $manager->persist($district);

        $district = new District();
        $district->setName('Drochia');
        $district->setShortCode('MD-DR');
        $manager->persist($district);

        $district = new District();
        $district->setName('Dubasari');
        $district->setShortCode('MD-DU');
        $manager->persist($district);

        $district = new District();
        $district->setName('Edinet');
        $district->setShortCode('MD-ED');
        $manager->persist($district);

        $this->addReference(3, $district);

        $district = new District();
        $district->setName('Falesti');
        $district->setShortCode('MD-FA');
        $manager->persist($district);

        $district = new District();
        $district->setName('Floresti');
        $district->setShortCode('MD-FL');
        $manager->persist($district);
        $this->addReference(5, $district);

        $district = new District();
        $district->setName('Gagauzia');
        $district->setShortCode('MD-GA');
        $manager->persist($district);

        $district = new District();
        $district->setName('Glodeni');
        $district->setShortCode('MD-GL');
        $manager->persist($district);

        $this->addReference(4, $district);

        $district = new District();
        $district->setName('Hincesti');
        $district->setShortCode('MD-HI');
        $manager->persist($district);

        $district = new District();
        $district->setName('Ialoveni');
        $district->setShortCode('MD-IA');
        $manager->persist($district);

        $this->addReference(6, $district);

        $district = new District();
        $district->setName('Leova');
        $district->setShortCode('MD-LE');
        $manager->persist($district);
        $this->addReference(14, $district);

        $district = new District();
        $district->setName('Nisporeni');
        $district->setShortCode('MD-NI');
        $manager->persist($district);

        $district = new District();
        $district->setName('Ocnita');
        $district->setShortCode('MD-OC');
        $manager->persist($district);

        $this->addReference(7, $district);

        $district = new District();
        $district->setName('Orhei');
        $district->setShortCode('MD-OR');
        $manager->persist($district);

        $district = new District();
        $district->setName('Rezina');
        $district->setShortCode('MD-RE');
        $manager->persist($district);

        $this->addReference(8, $district);

        $district = new District();
        $district->setName('Riscani');
        $district->setShortCode('MD-RI');
        $manager->persist($district);

        $district = new District();
        $district->setName('soldanesti');
        $district->setShortCode('MD-SD');
        $manager->persist($district);

        $this->addReference(13, $district);

        $district = new District();
        $district->setName('Singerei');
        $district->setShortCode('MD-SI');
        $manager->persist($district);

        $this->addReference(9, $district);

        $district = new District();
        $district->setName('Unitatea Teritoriala din Stinga Nistrului');
        $district->setShortCode('MD-SN');
        $manager->persist($district);

        $district = new District();
        $district->setName('Soroca');
        $district->setShortCode('MD-SO');
        $manager->persist($district);

        $district = new District();
        $district->setName('Straseni');
        $district->setShortCode('MD-ST');
        $manager->persist($district);

        $district = new District();
        $district->setName('Stefan Voda');
        $district->setShortCode('MD-SV');
        $manager->persist($district);

        $this->addReference(10, $district);

        $district = new District();
        $district->setName('Taraclia');
        $district->setShortCode('MD-TA');
        $manager->persist($district);

        $this->addReference(11, $district);

        $district = new District();
        $district->setName('Telenesti');
        $district->setShortCode('MD-TE');
        $manager->persist($district);

        $district = new District();
        $district->setName('Ungheni');
        $district->setShortCode('MD-UN');
        $manager->persist($district);

        $this->addReference(12, $district);

        $manager->flush();
    }
}
