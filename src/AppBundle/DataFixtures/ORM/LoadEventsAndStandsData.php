<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Company;
use AppBundle\Entity\Event;
use AppBundle\Entity\Stand;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadUserData
 */
class LoadEventsAndStandsData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Company[]
     */
    private $companies;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /* generate 10 companies */
        for ($i = 1; $i < 11; $i++) {
            $company = new Company();
            $company->setName($this->getRandomText(rand(8, 15)));
            $company->setContactDetails('+'.rand(1111111111, 9999999999).' email@company.com');
            $company->setLogoFile($this->getRandomCompanyLogo($i));
            $manager->persist($company);
            $this->companies[] = $company;
        }


        /* generate 35 events with in random district */
        for ($i = 0; $i < 35; $i++) {
            $event = new Event();
            $event->setDistrict($this->getReference(rand(1, 14)));

            $start = rand(2, 30);
            $event->setStartDate(new \DateTime("+$start days"));
            $end = $start + rand(1, 5);
            $event->setEndDate(new \DateTime("+$end days"));

            $event->setTitle($this->getRandomText(rand(10, 15)));
            $manager->persist($event);

            /* for each event generate random stands 5-8 */
            $this->createRandomStands($event, $manager);
        }

        $manager->flush();
    }

    /**
     * @param Event         $event
     * @param ObjectManager $manager
     */
    private function createRandomStands(Event $event, ObjectManager $manager)
    {
        $standsNumb = rand(5, 8);

        for ($i = 0; $i < $standsNumb; $i++) {
            $stand = new Stand();
            $stand->setEvent($event);
            $stand->setTitle($this->getRandomText(rand(18, 25)));
            $stand->setImageFile($this->getRandomStandImage());

            if ($company = $this->getCompanyOrNull()) {
                $stand->setCompany($company);
                $stand->setMarketingFile($this->getRandomStandImage());
            }

            $manager->persist($stand);
        }
    }

    /**
     * @return Company|null
     */
    private function getCompanyOrNull()
    {
        if (rand(0, 1)) {
            $company = next($this->companies);
            if ($company == false) {
                $company = reset($this->companies);
            }

            return $company;
        }

        return null;
    }

    /**
     * @param $length
     * @return string
     */
    private function getRandomText($length)
    {
        $start = rand(0, 400);
        $string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sagittis egestas odio, quis ultricies sem convallis at. Quisque feugiat sollicitudin arcu vitae pellentesque. Cras elementum porta consectetur. Etiam efficitur quis neque non sodales. Vivamus eu ipsum rhoncus, consectetur urna non, gravida magna. Morbi sit amet lobortis nulla, eu faucibus enim. Proin sit amet neque enim. Donec accumsan pulvinar blandit. Praesent tincidunt metus eu orci molestie feugiat.';

        $string = substr($string, $start);
        /* find positions of first uppercase char*/
        preg_match('/[A-Z]/', $string, $matches);
        $start = strpos($string, $matches[0]);

        return substr($string, $start, $length);
    }

    /**
     * @param integer $i
     * @return UploadedFile
     */
    private function getRandomCompanyLogo($i)
    {
        $original = __DIR__.'/images/company/'.$i.'_company.png';

        $filename = tempnam(sys_get_temp_dir(), 'company_logo_fixture');
        copy($original, $filename);

        return new UploadedFile($filename, null, null, null, null, true);
    }

    /**
     * @return UploadedFile
     */
    private function getRandomStandImage()
    {
        $original = __DIR__.'/images/stands/stand_'.rand(1, 5).'.jpg';

        $filename = tempnam(sys_get_temp_dir(), 'stand_image_fixture');
        copy($original, $filename);

        return new UploadedFile($filename, null, null, null, null, true);
    }
}
