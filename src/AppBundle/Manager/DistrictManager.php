<?php
namespace AppBundle\Manager;

use AppBundle\Repository\DistrictRepository;

/**
 * Class DistrictManager
 */
class DistrictManager
{
    /**
     * @var DistrictRepository
     */
    private $repository;

    /**
     * DistrictManager constructor.
     * @param DistrictRepository $repository
     */
    public function __construct(DistrictRepository $repository)
    {
        $this->repository = $repository;
    }
}
