<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use AppBundle\Repository\EventRepository;

/**
 * Class EventManager
 */
class EventManager
{
    /**
     * @var EventRepository
     */
    private $repository;

    /**
     * EventManager constructor.
     * @param EventRepository $repository
     */
    public function __construct(EventRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Event[]
     */
    public function getAvailableEvents()
    {
        return $this->repository->getAvailableEvents();
    }
}
