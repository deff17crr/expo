<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Company;
use AppBundle\Repository\CompanyRepository;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class CompanyManager
 */
class CompanyManager
{
    /**
     * @var CompanyRepository
     */
    private $repository;

    /**
     * @var UploaderHelper
     */
    private $vichUploaderHelper;

    /**
     * CompanyManager constructor.
     * @param CompanyRepository $repository
     * @param UploaderHelper    $uploaderHelper
     */
    public function __construct(CompanyRepository $repository, UploaderHelper $uploaderHelper)
    {
        $this->repository = $repository;
        $this->vichUploaderHelper = $uploaderHelper;
    }

    /**
     * @param Company $company
     * @return string
     */
    public function getPath(Company $company)
    {
        return $this->vichUploaderHelper->asset($company, 'logoFile');
    }

    /**
     * @param Company $company
     */
    public function save(Company $company)
    {
        $this->repository->save($company);
    }
}
