<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Event;
use AppBundle\Entity\Stand;
use AppBundle\Repository\StandRepository;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class StandManager
 */
class StandManager
{
    /**
     * @var StandRepository
     */
    private $repository;

    /**
     * @var UploaderHelper
     */
    private $vichUploaderHelper;

    /**
     * @var CacheManager
     */
    private $liipCacheManager;

    /**
     * StandManager constructor.
     * @param StandRepository $repository
     * @param CacheManager    $cacheManager
     * @param UploaderHelper  $uploaderHelper
     */
    public function __construct(StandRepository $repository, CacheManager $cacheManager, UploaderHelper $uploaderHelper)
    {
        $this->repository = $repository;
        $this->liipCacheManager = $cacheManager;
        $this->vichUploaderHelper = $uploaderHelper;
    }

    /**
     * @param integer $id
     * @return null|Stand
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @return null|object
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @param Stand $stand
     */
    public function save(Stand $stand)
    {
        $this->repository->save($stand);
    }

    /**
     * @param Event $event
     * @return Stand[]
     */
    public function getStandsByEvent(Event $event)
    {
        return $this->repository->getStandsByEvent($event);
    }

    /**
     * @param Stand  $stand
     * @param string $property
     * @return string
     */
    public function getPath(Stand $stand, $property = 'imageFile')
    {
        return $this->vichUploaderHelper->asset($stand, $property);
    }

    /**
     * @param string $path
     * @param string $filter
     * @return string
     */
    public function getThumbnail($path, $filter = 'stand_thumb')
    {
        return $this->liipCacheManager->getBrowserPath($path, $filter);
    }
}
