<?php
namespace AppBundle\Form;

use AppBundle\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ReservationStandType
 */
class ReservationStandType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('contactDetails', TextType::class, ['required' => true])
            ->add('logoFile', FileType::class, ['required' => true, 'label' => 'form_labels.company_logo'])
            ->add('marketingFile', FileType::class, [
                'required' => true,
                'mapped' => false,
                'label' => 'form_labels.marketing_file',
                'constraints' => [
                    new NotBlank(),
                    new File([
                        'maxSize' => "2M",
                        'mimeTypes' => ["application/pdf", "application/x-pdf", "image/png", "image/jpeg", "application/msword"],
                    ]),
                ],
            ])
            ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Company::class,
            'method' => 'PATCH',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'company';
    }
}
