<?php

namespace AppBundle\EventListener\Serializer;

use AppBundle\Entity\Event;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class EventSubscriber
 */
class EventSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'method' => 'onImageSerialized',
                'class' => Event::class,
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onImageSerialized(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var Event $eventEntity */
        $eventEntity = $event->getObject();

        $visitor->setData('district_short_code', $eventEntity->getDistrict()->getShortCode());
        $visitor->setData('district_name', $eventEntity->getDistrict()->getName());
    }
}
