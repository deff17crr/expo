<?php

namespace AppBundle\EventListener\Serializer;

use AppBundle\Entity\Company;
use AppBundle\Manager\CompanyManager;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class CompanySubscriber
 */
class CompanySubscriber implements EventSubscriberInterface
{
    /**
     * @var CompanyManager
     */
    private $companyManager;

    /**
     * CompanySubscriber constructor.
     * @param CompanyManager $companyManager
     */
    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'method' => 'onImageSerialized',
                'class' => Company::class,
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onImageSerialized(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var Company $company */
        $company = $event->getObject();

        $visitor->setData('logo_url', $path = $this->companyManager->getPath($company));
    }
}
