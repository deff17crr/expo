<?php

namespace AppBundle\EventListener\Serializer;

use AppBundle\Entity\Stand;
use AppBundle\Manager\StandManager;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class StandSubscriber
 */
class StandSubscriber implements EventSubscriberInterface
{
    /**
     * @var StandManager
     */
    private $standManager;

    /**
     * StandSubscriber constructor.
     * @param StandManager $standManager
     */
    public function __construct(StandManager $standManager)
    {
        $this->standManager = $standManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'method' => 'onImageSerialized',
                'class' => Stand::class,
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onImageSerialized(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var Stand $stand */
        $stand = $event->getObject();

        $visitor->setData('marketing_file_url', $path = $this->standManager->getPath($stand, 'marketingFile'));
        $visitor->setData('image_url', $path = $this->standManager->getPath($stand));
        $visitor->setData('image_thumb', $this->standManager->getThumbnail($path));
    }
}
