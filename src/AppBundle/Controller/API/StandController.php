<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Event;
use AppBundle\Entity\Stand;
use AppBundle\Form\ReservationStandType;
use AppBundle\Manager\StandManager;
use AppBundle\Utils\FormErrorsHelper;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StandController
 */
class StandController extends BaseRestController
{
    /**
     * @Get("/events/{event}/stands", options={"expose"=true})
     * @param Event $event
     * @return Response
     */
    public function getStandsEventAction(Event $event)
    {
        try {
            $albums = $this->getManager()->getStandsByEvent($event);

            return $this->response($albums, null, []);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Get("/stands/{stand}", options={"expose"=true})
     * @param Stand $stand
     * @return Response
     */
    public function getStandAction(Stand $stand)
    {
        try {
            /* check if stand isn't already booked */
            if (null !== $stand->getCompany()) {
                throw $this->createNotFoundException();
            }

            return $this->response($stand, null, []);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @param Stand   $stand
     * @param Request $request
     * @return Response
     * @Patch(options={"expose"=true})
     */
    public function patchStandReservationAction(Stand $stand, Request $request)
    {
        try {
            /* check if stand isn't already booked */
            if (null !== $stand->getCompany()) {
                throw $this->createNotFoundException();
            }

            $form = $this->createForm(ReservationStandType::class);
            $form->handleRequest($request);
            if ($form->isValid()) {
                /* save new company */
                $stand->setCompany($form->getData());
                $stand->setMarketingFile($form->get('marketingFile')->getData());
                $this->getManager()->save($stand);
            } else {
                $formErrors = FormErrorsHelper::getErrorsAsArray($form);

                return $this->response(['formErrors' => $formErrors], 400);
            }

            return $this->response($stand, null, []);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @return StandManager
     */
    private function getManager()
    {
        return $this->get('app.stand.manager');
    }
}
