<?php

namespace AppBundle\Controller\API;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseRestController
 */
class BaseRestController extends FOSRestController
{
    /**
     * @param null  $data
     * @param null  $statusCode
     * @param array $headers
     * @param array $groups
     * @return Response
     */
    public function response($data = null, $statusCode = null, array $headers = array(), array $groups = [])
    {
        if ($groups) {
            $context = SerializationContext::create()->setGroups($groups);
            $view = $this->view($data, $statusCode, $headers)->setSerializationContext($context);

            return $this->handleView($view);
        }

        return $this->handleView($this->view($data, $statusCode, $headers));
    }
}
