<?php

namespace AppBundle\Controller\API;

use AppBundle\Manager\EventManager;
use FOS\RestBundle\Controller\Annotations\Get;

/**
 * Class EventController
 */
class EventController extends BaseRestController
{
    /**
     * @return \FOS\RestBundle\View\View
     * @Get("/events", options={"expose"=true})
     */
    public function getEventsAction()
    {
        try {
            $albums = $this->getManager()->getAvailableEvents();

            return $this->response($albums, null, [], ['index', 'Default']);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @return EventManager
     */
    private function getManager()
    {
        return $this->get('app.event.manager');
    }
}
