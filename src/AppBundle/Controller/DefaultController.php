<?php
namespace AppBundle\Controller;

use AppBundle\Form\ReservationStandType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @return Response
     */
    public function indexAction()
    {
        $reservationStandType = $this->createForm(ReservationStandType::class);

        return $this->render('AppBundle:Default:index.html.twig', [
            'reservationStandForm' => $reservationStandType->createView(),
        ]);
    }
}
