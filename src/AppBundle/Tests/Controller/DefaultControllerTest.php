<?php
namespace AppBundle\Tests\Controller;

use AppBundle\Manager\StandManager;
use AppBundle\Tests\AbstractKernelTestCase;

/**
 * Class DefaultControllerTest
 */
class DefaultControllerTest extends AbstractKernelTestCase
{
    /**
     * @var StandManager
     */
    private $standManager;

    /**
     * Set up tests
     */
    protected function setUp()
    {
        $this->setBrowser("firefox");
        $this->setBrowserUrl("http://expo.local/");

        static::bootKernel();
        $this->standManager = self::$kernel->getContainer()->get('app.stand.manager');
    }

    /**
     * Test event map page
     */
    public function testEventsMapPage()
    {
        $this->url('/app_dev.php');

        /* wait for loading events */
        $this->waitUntil(function () {
            if ($this->byCssSelector('#event-map .event')) {
                return true;
            }

            return null;
        }, 5000);

        $map = $this->byId('event-map');

        /* At start all districts should be inactive */
        $this->assertFalse($this->elementExist($map, 'selected'), 'Selected districts shouldn\'t exist');
        $this->assertFalse($this->elementExist($map, 'hovered'), 'Hovered districts shouldn\'t exist');

        /* At start link to stands should be disabled. */
        $classAttr = $this->byClassName('event-map-button')->attribute('class');
        $this->assertNotFalse(strpos($classAttr, 'disabled'), 'At start link to stands should be disabled.');

        /* select random event */
        $event = $map->byCssSelector('.event:nth-child('.rand(3, 25).')');
        $event->click();

        $district = $map->byId($event->attribute('district-short_code'));
        $this->assertNotFalse(strpos($district->attribute('class'), 'selected'), 'When click on event, appropriate district should be mark as selected');

        /* check if button is working */
        $buttonToStandsList = $this->byClassName('event-map-button');
        $buttonToStandsList->click();
        $this->waitUntil(function () {
            if ($this->byClassName('stand')) {
                return true;
            }

            return null;
        }, 5000);
    }

    /**
     * Test page with list of stands
     */
    public function testStandsListPage()
    {
        $selectRandomEvent = function () {
            $this->url('/app_dev.php');

            /* wait for loading events */
            $this->waitUntil(function () {
                if ($this->byCssSelector('#event-map .event')) {
                    return true;
                }

                return null;
            }, 5000);

            $map = $this->byId('event-map');

            /* select random event */
            $event = $map->byCssSelector('.event:nth-child('.rand(3, 25).')');
            $event->click();

            /* and go to stands of this event */
            $buttonToStandsList = $this->byClassName('event-map-button');
            $buttonToStandsList->click();
            $this->waitUntil(function () {
                if ($this->byClassName('stand')) {
                    return true;
                }

                return null;
            }, 5000);
        };

        /* select event with booked and free stands */
        $selectRandomEvent();
        while (!$this->elementExist($this, '.stand .btn.btn-info') || !$this->elementExist($this, '.stand .booked')) {
            $selectRandomEvent();
        }

        /* check if stands in ui meets with entrie in database */
        $freeStandButton = $this->byCssSelector('.stand .btn.btn-info');
        $stand = $this->standManager->find($freeStandButton->attribute('data-stand-id'));
        $this->assertNull($stand->getCompany());

        /* check if stands in ui meets with entrie in database */
        $freeStandButton = $this->byCssSelector('.stand .booked');
        $stand = $this->standManager->find($freeStandButton->attribute('data-stand-id'));
        $this->assertNotNull($stand->getCompany());

        /* test fancybox */
        $this->byCssSelector('.stand img')->click();
        $this->waitUntil(function () {
            if ($this->byCssSelector('.fancybox-close')) {
                return true;
            }

            return null;
        }, 2000);

        $this->byCssSelector('.fancybox-close')->click();
        sleep(1);

        /* test "go to reservation page" button */
        $this->byCssSelector('.stand .btn.btn-info')->click();
        $this->waitUntil(function () {
            if ($this->byCssSelector('.reservation-stand-form')) {
                return true;
            }

            return null;
        }, 5000);

        $this->assertTrue($this->elementExist($this, '.reservation-stand-form'));
        $this->byCssSelector('.reservation-stand-header .btn')->click();
        $this->waitUntil(function () {
            if ($this->byCssSelector('.stands-header .btn')) {
                return true;
            }

            return null;
        }, 3000);

        /* test back to events map button */
        $backButton = $this->byCssSelector('.stands-header .btn');
        $backButton->click();

        $this->waitUntil(function () {
            if ($this->byCssSelector('#event-map .event')) {
                return true;
            }

            return null;
        }, 5000);

        $this->assertTrue($this->elementExist($this, '#event-map .event'));
    }


    /**
     * Test Reservation page
     */
    public function testReservationPage()
    {
        $selectRandomEvent = function () {
            $this->url('/app_dev.php');

            /* wait for loading events */
            $this->waitUntil(function () {
                if ($this->byCssSelector('#event-map .event')) {
                    return true;
                }

                return null;
            }, 5000);

            $map = $this->byId('event-map');

            /* select random event */
            $event = $map->byCssSelector('.event:nth-child('.rand(3, 25).')');
            $event->click();

            /* and go to stands of this event */
            $buttonToStandsList = $this->byClassName('event-map-button');
            $buttonToStandsList->click();
            $this->waitUntil(function () {
                if ($this->byClassName('stand')) {
                    return true;
                }

                return null;
            }, 5000);
        };

        /* select event with booked and free stands */
        $selectRandomEvent();
        while (!$this->elementExist($this, '.stand .btn.btn-info') || !$this->elementExist($this, '.stand .booked')) {
            $selectRandomEvent();
        }

        /* go to reservation page */
        $this->byCssSelector('.stand .btn.btn-info')->click();
        $this->waitUntil(function () {
            if ($this->byClassName("reservation-stand-form")) {
                return true;
            }

            return null;
        }, 5000);

        $reservationForm = $this->byClassName("reservation-stand-form");

        $logoFileInput = $reservationForm->byCssSelector('[name="company[logoFile]"]');
        $marketingFile = $reservationForm->byCssSelector('[name="company[marketingFile]"]');
        $nameInput = $reservationForm->byCssSelector('[name="company[name]"]');
        $contactDetails = $reservationForm->byCssSelector('[name="company[contactDetails]"]');

        $logoWidgetContainer = $reservationForm->byClassName('company-logo-group');
        $marketingFileWidgetContainer = $reservationForm->byClassName('company-marketing-file-group');
        $nameWidgetConatiner = $reservationForm->byClassName('company-name-group');
        $contactDetailsWidget = $reservationForm->byClassName('contact-details-group');

        /* form test case one */
        $logoFileInput->value(__DIR__.'/../files_for_test/big_image.jpg');
        $marketingFile->value(__DIR__.'/../files_for_test/invalid_format.zip');
        $reservationForm->byLinkText('Submit')->click();

        $this->waitUntil(function () {
            if ($this->byCssSelector(".reservation-stand-form .text-danger")) {
                return true;
            }

            return null;
        }, 5000);

        $expectedMessage = 'The image width is too big (1600px). Allowed maximum width is 150px.';
        $this->assertEquals($expectedMessage, $logoWidgetContainer->byClassName('text-danger')->text());

        $expectedMessage = 'The mime type of the file is invalid ("application/zip"). Allowed mime types are "application/pdf", "application/x-pdf", "image/png", "image/jpeg", "application/msword".';
        $this->assertEquals($expectedMessage, $marketingFileWidgetContainer->byClassName('text-danger')->text());

        $expectedMessage = 'This value should not be blank.';
        $this->assertEquals($expectedMessage, $nameWidgetConatiner->byClassName('text-danger')->text());

        $expectedMessage = 'This value should not be blank.';
        $this->assertEquals($expectedMessage, $contactDetailsWidget->byClassName('text-danger')->text());

        /* form test case one */
        $logoFileInput->clear();
        $logoFileInput->value(__DIR__.'/../files_for_test/test_logo.png');
        $marketingFile->clear();
        $marketingFile->value(__DIR__.'/../files_for_test/test_file.doc');
        $nameInput->value('Test company X');
        $contactDetails->value('+1 555 888555  email@example.com');
        $reservationForm->byLinkText('Submit')->click();

        $this->waitUntil(function () {
            if ($this->byClassName("stand")) {
                return true;
            }

            return null;
        }, 5000);

        $this->assertTrue($this->elementExist($this, '.stand'));
    }

    /**
     * @param \PHPUnit_Extensions_Selenium2TestCase_Element|DefaultControllerTest $element
     * @param string                                        $selector
     * @return boolean
     */
    private function elementExist($element, $selector)
    {
        try {
            $element->byCssSelector($selector);

            return true;
        } catch (\PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            return false;
        }
    }
}
