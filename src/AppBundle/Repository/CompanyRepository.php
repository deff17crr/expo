<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Company;
use Doctrine\ORM\EntityRepository;

/**
 * Class CompanyRepository
 */
class CompanyRepository extends EntityRepository
{
    /**
     * @param Company $company
     */
    public function save(Company $company)
    {
        $this->_em->persist($company);
        $this->_em->flush();
    }
}
