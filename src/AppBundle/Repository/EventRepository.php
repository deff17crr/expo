<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Event;
use Doctrine\ORM\EntityRepository;

/**
 * Class EventRepository
 */
class EventRepository extends EntityRepository
{
    /**
     * @return Event[]
     */
    public function getAvailableEvents()
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.district', 'd')
            ->where('e.startDate >= :now')
            ->andWhere('e.startDate <= :plus_month')
            ->setParameters([
                'now' => new \DateTime('now'),
                'plus_month' => new \DateTime('+1 month'),
            ])
            ->getQuery()
            ->getResult();
    }
}
