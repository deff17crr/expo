<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Event;
use AppBundle\Entity\Stand;
use Doctrine\ORM\EntityRepository;

/**
 * Class StandRepository
 */
class StandRepository extends EntityRepository
{
    /**
     * @param Stand $stand
     */
    public function save(Stand $stand)
    {
        $this->_em->persist($stand);
        $this->_em->flush();
    }

    /**
     * @param Event $event
     * @return Stand[]
     */
    public function getStandsByEvent(Event $event)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.event', 'e')
            ->leftJoin('s.company', 'c')
            ->where('e.id = :event')
            ->setParameters(['event' => $event->getId()])
            ->getQuery()
            ->getResult();
    }
}
